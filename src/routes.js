'use strict';

const { json } = require('express');

module.exports = function(app){
  const jsonku = require('./controller');

  app.route('/')
    .get(jsonku.index);

  app.route('/tampil')
    .get(jsonku.tampilSemuaMahasiswa);
  
  app.route('/tampil/:id')
    .get(jsonku.tampilDataById);

  app.route('/tambah')
    .post(jsonku.inputDataMahasiswa);
  
  app.route('/update')
    .put(jsonku.updateDataMahasiswa);

  app.route('/delete')
    .delete(jsonku.deleteDataMahasiswa);
    
}