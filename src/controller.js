'use strict';

var response = require('./res');
var connection = require('./koneksi');

exports.index = function(req, res){
  response.ok("Aplikasi Rest API normal", res);
};

//menampilkan semua data mahasiswa
exports.tampilSemuaMahasiswa = function(req, res){
  connection.query('select * from mahasiswa', function (error, rows, fields){
    if(error){
      console.log(error);
    }else{
      response.ok(rows, res);
    }
  });
};

//tampil data berdasarkan id mahasiswa tertentu
exports.tampilDataById = function(req, res){
  const id = req.params.id;
  const query_sql = 'SELECT * FROM mahasiswa WHERE nim = ?';
  connection.query(query_sql, [id], function(err, result, fields){
    if(err){
      console.log(err);
    }else{
      response.ok(result, res);
    }
  });
};

//input data mahasiswa
exports.inputDataMahasiswa = function(req, res){
  const nim = req.body.nim;
  const nama = req.body.nama;
  const jurusan = req.body.jurusan;
  if(typeof nim !== 'undefined'){
    const query_insert = 'INSERT INTO mahasiswa(nim,nama,jurusan) VALUES(?,?,?)';
    connection.query(query_insert, [nim,nama,jurusan], function(err, result, fields){
      if(err){
        console.log(err);
      }else{
        response.ok("Berhasil Menambahkan Data!", res)
      }
    });
  }else{
    response.fail("data gagal disimpan, nim tidak ada", res)
  }
};

//ubah data berdasarkan data mahasiswa
exports.updateDataMahasiswa = function(req, res){
  const id = req.body.id_mahasiswa;
  const nim = req.body.nim;
  const nama = req.body.nama;
  const jurusan = req.body.jurusan;
  const query_sql = 'SELECT * FROM mahasiswa WHERE id_mahasiswa = ?';
  connection.query(query_sql, [id], function(err, result, fields){
    if(result.length > 0){
      const query_update = 'update mahasiswa set nim=?, nama=?, jurusan=? where id_mahasiswa = ? ';
      if(typeof id !== 'undefined'){
        connection.query(query_update, [nim,nama,jurusan,id], function(err, result, fields){
          if(err){
            console.log(err);
          }else{
            response.ok('Data berhasil di update', res)
          }
        });
      }else{
        response.fail("data gagal di update", res)
      }
    }else{
      response.fail("data gagal di update karena data mahasiswa tidak ditemukan", res)
    }
  });
};

//delete data mahasiswa
exports.deleteDataMahasiswa = function(req, res){
  const id = req.body.id_mahasiswa;
  const query_sql = 'SELECT * FROM mahasiswa WHERE id_mahasiswa = ?';
  connection.query(query_sql, [id], function(err, result, fields){
    if(result.length > 0 && typeof id !== 'undefined'){
      const query_delete = 'delete from mahasiswa where id_mahasiswa = ?';
        connection.query(query_delete, [id], function(err, result, fields){
          if(err){
            console.log(err);
          }else{
            response.ok('data berhasil di hapus', res)
          }
        });
    }else{
      response.fail('data gagal dihapus', res)
    }
  });
};